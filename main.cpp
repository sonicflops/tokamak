#include <QApplication>
#include <iostream>

#include "audio/osc.hpp"
#include "audio/analysis.hpp"
int main(int argc, char *argv[]) {
// Q_INIT_RESOURCE(resfile);
 QApplication app(argc, argv);
//return app.exec();

    // 60Hz wave @ 44100Hz sample rate
    auto fc = 500;
    auto fs = 44100;
    auto periods = 2u;

    auto frames = 150; // run through a 150 frame cycle
    toka::osc::sine sineosc(fc,fs, frames);
    toka::analysis::echo_channel ec;

    auto l = toka::channel_uptr(new toka::f32sample[ frames ]);
    auto r = toka::channel_uptr(new toka::f32sample[ frames ]);

    toka::signal signal(std::move(l), std::move(r), frames);
    signal.pass(sineosc); // Generate a sine wave for this cycle

    return 0;
}
