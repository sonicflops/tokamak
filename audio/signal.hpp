/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Signal.hpp
 * Author: cfg
 *
 * Created on 21 January 2017, 17:17
 */

#ifndef SIGNAL_HPP
#define SIGNAL_HPP

#include <array>

#include "audio_common.hpp"


namespace toka {

class signal {
public:
  using passop = std::function<void(signal&)>;
public:
  signal();
  signal(channel_uptr left, channel_uptr right, frame_count = TOKA_PERIOD_SIZE);
  signal(const signal& orig);
  virtual ~signal();
  
  signal_channel channel(stereo_channel index);
  frame_count num_frames();
  
  
  template<typename T>
  void pass(T& op) {
    op(*this);
  }

private:
  channel_pair _channels;
  frame_count _frames;
};

}
#endif /* SIGNAL_HPP */

