#ifndef OSC_HPP
#define OSC_HPP
#include "pass_common.hpp"

namespace toka { namespace osc {

  class sine {
  public:
    sine(float fc, int fs = 44100, float a = 1.0)
      : _fs(fs),
        _fc(fc),
        _a(a),
        _x(0.0),
        _dt(1.0/_fs),
        _2pi(2.0*M_PI),
        _2pifc(_2pi * _fc)
    { }
    
    void operator()(signal& signal) {
      auto l = signal.channel(stereo_channel::left);
      auto r = signal.channel(stereo_channel::right);

      auto len = signal.num_frames();
      
      for(auto i = 0u; i < len; i++) {
        auto y = std::sin(_2pifc * _x++ / _fs );
        l[i] = y;
        r[i] = y;
      }
      
      
            
    }

  private:
    int   _fs;    ///< Sample rate
    float _fc,    ///< Frequency of wave
          _a,     ///< Amplitude
          _x,     ///< Current x index
          _dt,    ///< Time delta of sample
          _2pi,   ///< 2 * pi   
          _2pifc; ///< 2 * pi * fs
  };

} }


#endif /* OSC_HPP */

