#ifndef AUDIO_COMMON_HPP
#define AUDIO_COMMON_HPP
#include <array>
#include <vector>
#include <memory>
#include <cmath>
#include "gsl/gsl"

#define TOKA_PERIOD_SIZE 512
#define TOKA_NUM_CHANNELS 2

namespace toka {

  using f32sample = float;
  using f32sample_uptr = std::unique_ptr<f32sample>;
  
  using frame_count = std::uint32_t;
  using sample_count = std::uint32_t;
  
  enum class stereo_channel : int {
    left, right
  };
  
  template<typename T>
  inline int strong_cast(T v) {
    return static_cast<int>(v);
  }
  
  using signal_channel = f32sample*;
  using channel_uptr = std::unique_ptr<f32sample[]>;
  using channel_pair = std::array<signal_channel, TOKA_NUM_CHANNELS>;
  using channel_span = gsl::span<f32sample, TOKA_PERIOD_SIZE>;
}


#endif /* AUDIO_COMMON_HPP */

