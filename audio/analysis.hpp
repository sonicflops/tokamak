#ifndef ANALYSIS_HPP
#define ANALYSIS_HPP
#include "signal.hpp"


namespace toka { namespace analysis {
 
  class echo_channel {
  public:
    echo_channel(stereo_channel channel = stereo_channel::left)
      : _channel(channel) { }
      
    void operator() (signal& signal) {
      auto channel = signal.channel(_channel);
      auto len = signal.num_frames();
      for(auto  i = 0u; i < len; i++) {
        std::cout << channel[i] << std::endl;
      }
    }
            
  private:
    stereo_channel _channel;
  };
  
} }
#endif /* ANALYSIS_HPP */

