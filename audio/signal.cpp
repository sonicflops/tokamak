#include "signal.hpp"
using namespace toka;
signal::signal() 
  : _channels{ nullptr, nullptr }, _frames(0)
{ }
  
signal::signal(channel_uptr left, channel_uptr right, frame_count num_frames) 
  : _channels{ left.release(), right.release() }, _frames(num_frames)
{ }


signal::signal(const signal& orig)
  : _channels(orig._channels), _frames(orig._frames)
{ }

signal::~signal() {
}

signal_channel signal::channel(stereo_channel index) {
  return _channels[strong_cast(index)];
}

frame_count signal::num_frames() {
  return _frames;
}
